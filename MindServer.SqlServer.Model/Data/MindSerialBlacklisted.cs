﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MindServer.SqlServer.Model.Data
{
    [Table("BlackList")]
    public class MindSerialBlacklisted
    {
        public MindSerialBlacklisted()
        {
            Id = -1;
        }

        public MindSerialBlacklisted(int id, string serial, string reason)
        {
            Id = id;
            Serial = serial;
            Reason = reason;
        }

        [Key]
        [Column("BlackListId")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("Serial")]
        public string Serial { get; set; }

        [Column("Reason")]
        public string Reason { get; set; }
    }
}
