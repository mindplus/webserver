﻿using System;

namespace MindServer.ServerCommunication.Exceptions
{
    public class ServerCommunicationDataAccessEngineException : Exception
    {
        public Exception Exception { get; protected set; }
        public string Message { get; protected set; }
        public ServerCommunicationDataAccessEngineException(string message, Exception ex)
        {
            Exception = ex;
            Message = message;
        }
    }
}
