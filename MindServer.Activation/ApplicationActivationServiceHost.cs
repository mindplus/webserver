﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Channels;
using System.Threading;
using System.Xml.Linq;
using CommunicationEngine;
using CommunicationEngine.Exceptions;
using CommunicationEngine.Utils;
using Data;
using Data.Helpers;
using MindGateEEG.Communication.WcfCommunication.Activation;
using MindGateEEG.Communication.WcfCommunication.Activation.Contracts;
using MindGateEEG.Communication.WcfCommunication.Activation.Exceptions;
using MindGateEEG.Communication.WcfCommunication.Activation.Messages;
using MindGateEEG.Communication.WcfCommunication.Activation.Serialization;
using MindGateEEG.Communication.WcfCommunication.Management.Contracts;
using MindServer.Activation.Data;
using MindServer.SqlServer.Model.Data;

namespace MindServer.Activation
{
    /// <summary>
    /// Activation host service
    /// TODO: remove from sended to client assembly, and make it only on server
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class ApplicationActivationServiceHost : IApplicationActivation
    {
        #region properties
        private CommunicationServiceHost<IApplicationActivation> _host;
        private readonly string _addressRoot;
        #endregion

        public ApplicationActivationServiceHost(string root)
        {
            _addressRoot = root;
        }

        public ApplicationActivationServiceHost()
        {
        }

        #region initialization
        /// <summary>
        /// call only in windows environment
        /// </summary>
        public void InitializeCommunication()
        {
            string endpointSuffix = Definitions.ServiceName;

            // winforms
            if (Environment.UserInteractive)
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback(
                                                 (obj) =>
                                                 {
                                                     _host =
                                                         new CommunicationServiceHost
                                                             <IApplicationActivation>(_addressRoot,
                                                                                                    endpointSuffix, BindingType.WsHttp);
                                                     _host.Initialize(this);
                                                     _host.Open();
                                                 }));
            }
            else
            {
                _host = new CommunicationServiceHost<IApplicationActivation>(_addressRoot, endpointSuffix, BindingType.WsHttp);
                _host.Initialize(this);
                _host.Open();
            }

        }

        public void Dispose()
        {

            if (_host != null)
            {
                _host.Close();
                _host = null;
            }

        }
        #endregion

        #region activation
        public ActivationResponse ActivateApplication(ActivationRequest activationRequest)
        {
            try
            {
                // processing input block - decode request
                string xml = activationRequest.ActivationRequestMessage.Data.GetString();
                xml = CryptoHelper.DecryptStringAES(xml, CryptoHelper.SHAREDSECRET);
                XDocument document = xml.DeserializeToXDocument();

                ActivationRequestMessageContent requestContent =
                    document.DeserializeToActivationRequestMessageContent();

                // verification of request data
                if (!activationRequest.ApplicationName.Equals(requestContent.ApplicationTypeVerification))
                    throw new FaultException<ActivationEngineFault>(new ActivationEngineFault("Request data verification failed"), "Request data verification failed", new FaultCode("verification"));
                
                if (String.IsNullOrEmpty(requestContent.Guid))
                    throw new FaultException<ActivationEngineFault>(new ActivationEngineFault("Request data activation error"), "Request data activation error", new FaultCode("guid"));

                // processing registration block
                try
                {
                    string connectionString = ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                    ActivationFactory factory = new ActivationFactory(connectionString);
                    ActivationOutput output = factory.ActivateApplication(requestContent.Guid, requestContent.SerialNumber);

                    // processing out block, set registration data and encode response
                    ActivationResponseMessageContent message = new ActivationResponseMessageContent();
                    message.Guid = output.Guid;
                    message.IsActivationNeeded = output.IsActivationNeeded;
                    message.ApplicationType = requestContent.ApplicationTypeVerification;
                    message.KeyInfo = KeyInfo.KeyOk;

                    if (output.IsKeyExpired)
                        message.KeyInfo = KeyInfo.KeyExpired;
                    if (output.IsKeyBlacklisted)
                        message.KeyInfo= KeyInfo.KeyBlacklisted;
                    if (!output.IskeyValid)
                        message.KeyInfo = KeyInfo.KeyInvalid;

                    XDocument documentOut = message.SerializeToDocument();
                    string xmlOut = documentOut.SerializeToXml();
                    xmlOut = CryptoHelper.EncryptStringAES(xmlOut, CryptoHelper.SHAREDSECRET);
                    byte[] messageDataOut = xmlOut.GetBytes();

                    if (!output.IsActivationNeeded)
                        return new ActivationResponse(ActivationReturnType.Activated, "0", new ActivationResponseMessage(messageDataOut));
                    else
                        return new ActivationResponse(ActivationReturnType.NotActivated, "0", new ActivationResponseMessage(messageDataOut));

                }
                catch (ActivationDataAccessEngineException ex)
                {
                    throw new FaultException<ActivationEngineFault>(new ActivationEngineFault("Cannot register, error occured"), ex.Message + ", " + (ex.Exception != null ? ex.Exception.Message : "no inner message"), new FaultCode("db"));
                    //throw new ActivationEngineException("Cannot register, error occured", ex);
                }

            }
            catch (FaultException<ActivationEngineFault> ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new FaultException<ActivationEngineFault>(new ActivationEngineFault("Cannot register, unknown processing error occured"), ex.Message + ", " + (ex.InnerException != null ? ex.InnerException.Message : "no inner message"), new FaultCode("unknown"));
            }
        }

        public CheckActivationResponse CheckApplicationActivation(CheckActivationRequest checkActivationRequest)
        {
            try
            {
                // processing input block - decode request
                string xml = checkActivationRequest.CheckActivationRequestMessage.Data.GetString();
                xml = CryptoHelper.DecryptStringAES(xml, CryptoHelper.SHAREDSECRET);
                XDocument document = xml.DeserializeToXDocument();

                CheckActivationRequestMessageContent requestContent =
                    document.DeserializeToCheckActivationRequestMessageContent();

                // verification of request data
                if (!checkActivationRequest.ApplicationName.Equals(requestContent.ApplicationTypeVerification))
                    throw new FaultException<ActivationEngineFault>(new ActivationEngineFault("Request data verification failed"), "Request data verification failed", new FaultCode("verification"));
                //					throw new ActivationEngineException("Request data verification failed", null);

                // processing registration block
                try
                {
                    string connectionString = ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                    ActivationFactory factory = new ActivationFactory(connectionString);
                    bool output = factory.IsApplicationRegisteredAndActivated(requestContent.Guid);

                    // processing out block, set registration data and encode response
                    CheckActivationResponseMessageContent message = new CheckActivationResponseMessageContent();
                    message.Guid = requestContent.Guid;
                    message.IsActivationNeeded = !output;
                    message.ApplicationType = requestContent.ApplicationTypeVerification;

                    XDocument documentOut = message.SerializeToDocument();
                    string xmlOut = documentOut.SerializeToXml();
                    xmlOut = CryptoHelper.EncryptStringAES(xmlOut, CryptoHelper.SHAREDSECRET);
                    byte[] messageDataOut = xmlOut.GetBytes();

                    if (!output)
                        return new CheckActivationResponse(CheckActivationReturnType.Activated, "0", new CheckActivationResponseMessage(messageDataOut));
                    else
                        return new CheckActivationResponse(CheckActivationReturnType.NotActivated, "0", new CheckActivationResponseMessage(messageDataOut));

                }
                catch (ActivationDataAccessEngineException ex)
                {
                    throw new FaultException<ActivationEngineFault>(new ActivationEngineFault("Cannot register, error occured"), ex.Message + ", " + (ex.InnerException != null ? ex.InnerException.Message : "no inner message"), new FaultCode("db"));
                    //throw new ActivationEngineException("Cannot register, error occured", ex);
                }

            }
            catch (FaultException<ActivationEngineFault> ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new FaultException<ActivationEngineFault>(new ActivationEngineFault("Cannot register, unknown processing error occured"), ex.Message + ", " + (ex.InnerException != null ? ex.InnerException.Message : "no inner message"), new FaultCode("unknown"));
            }
        }

        #endregion

        #region registration
        public RegistrationResponse RegisterApplication(RegistrationRequest registrationRequest)
        {
            try
            {
                // retrieve client public ip
                string ip;
                int port;
                bool investigatedIp = ServiceHelper.RetrieveClientPublicIpAddress(out ip, out port);

                if (registrationRequest.Port != null)
                    port = registrationRequest.Port.Value;

                // processing input block - decode request
                string xml = registrationRequest.RegistrationRequestMessage.Data.GetString();
                xml = CryptoHelper.DecryptStringAES(xml, CryptoHelper.SHAREDSECRET);
                XDocument document = xml.DeserializeToXDocument();

				RegistrationRequestMessageContent requestContent =
                    document.DeserializeToRegistrationRequestMessageContent();

				// verification of request data
				if (!registrationRequest.ApplicationName.Equals(requestContent.ApplicationTypeVerification))
					throw new FaultException<ActivationEngineFault>(new ActivationEngineFault("Request data verification failed"), "Request data verification failed", new FaultCode("verification"));
//					throw new ActivationEngineException("Request data verification failed", null);

                // processing registration block
				try
				{
				    string connectionString = ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
					ActivationFactory factory = new ActivationFactory(connectionString);

                    if (investigatedIp)
                        requestContent.PublicIpAddress = ip + ":" + port;

					RegistrationOutput output = factory.RegisterApplication(
						registrationRequest.ActivationDate, requestContent.IpAddress, requestContent.PublicIpAddress, requestContent.ApplicationTypeVerification, requestContent.HardwareData);

					// processing out block, set registration data and encode response
					RegistrationResponseMessageContent message = new RegistrationResponseMessageContent();
					message.Guid = output.Guid;
					message.IsActivationNeeded = output.IsActivationNeeded;
					message.ApplicationType = requestContent.ApplicationTypeVerification;
				    message.IpAddress = requestContent.IpAddress;
				    message.PublicIpAddress = requestContent.PublicIpAddress;

                    if (!output.IsNewRegistration)
                    {
                        message.Therapeutists = new List<ComTherapeutist>();

                        foreach (DbTherapeutist login in factory.GetApplicationUsers(output.Guid))
                        {
                            message.Therapeutists.Add(new ComTherapeutist(login.Id, login.Login,login.Password));
                        }
                    }

				    XDocument documentOut = message.SerializeToDocument();
					string xmlOut = documentOut.SerializeToXml();
					xmlOut = CryptoHelper.EncryptStringAES(xmlOut, CryptoHelper.SHAREDSECRET);
					byte[] messageDataOut = xmlOut.GetBytes();

					if (output.IsNewRegistration)
						return new RegistrationResponse(RegistrationReturnType.NewRegistered, "0", new RegistrationResponseMessage(messageDataOut));
					else
						return new RegistrationResponse(RegistrationReturnType.AlreadyRegistered, "0", new RegistrationResponseMessage(messageDataOut));
					
				}
				catch(ActivationDataAccessEngineException ex)
				{
                    throw new FaultException<ActivationEngineFault>(new ActivationEngineFault("Cannot register, error occured"), ex.Message + ", " + (ex.InnerException != null ? ex.InnerException.Message : "no inner message"), new FaultCode("db"));
					//throw new ActivationEngineException("Cannot register, error occured", ex);
				}

            }
			catch (FaultException<ActivationEngineFault> ex)
			{
				throw;
			}
            catch (Exception ex)
            {
                throw new FaultException<ActivationEngineFault>(new ActivationEngineFault("Cannot register, unknown processing error occured"), ex.Message + ", " + (ex.InnerException != null ? ex.InnerException.Message : "no inner message"), new FaultCode("unknown"));
            }
        }

        public DiscoverApplicationResponse DiscoverApplication(DiscoverApplicationRequest discoverRequest)
        {
            try
            {
                // processing input block - decode request
                string xml = discoverRequest.DiscoverApplicationRequestMessage.Data.GetString();
                xml = CryptoHelper.DecryptStringAES(xml, CryptoHelper.SHAREDSECRET);
                XDocument document = xml.DeserializeToXDocument();

                DiscoverApplicationRequestMessageContent requestContent =
                    document.DeserializeToDiscoverApplicationRequestMessageContent();

                // verification of request data
                if (!discoverRequest.ApplicationName.Equals(requestContent.ApplicationTypeVerification))
                    throw new FaultException<ActivationEngineFault>(new ActivationEngineFault("Request data verification failed"), "Request data verification failed", new FaultCode("verification"));
                //					throw new ActivationEngineException("Request data verification failed", null);

                // processing discovery block
                try
                {
                    string connectionString = ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                    ActivationFactory factory = new ActivationFactory(connectionString);
                    DiscoverOutput output = factory.DiscoverApplication(requestContent.Guid, requestContent.TherapeutistName, requestContent.PatientLogin, requestContent.PatientPassword);

                    // processing out block, set registration data and encode response
                    DiscoverApplicationResponseMessageContent message = new DiscoverApplicationResponseMessageContent();
                    message.Guid = output.Guid;
                    message.PublicIpAddress = output.PublicIpAddress;
                    message.InternalIpAddress = output.InternalIpAddress;
                    message.LoggedPatientId = output.LoggedPatientId;
                    message.TherapeutistId = output.TherapeutistId;
                    message.TherapeutistLogin = output.TherapeutistLogin;

                    XDocument documentOut = message.SerializeToDocument();
                    string xmlOut = documentOut.SerializeToXml();
                    xmlOut = CryptoHelper.EncryptStringAES(xmlOut, CryptoHelper.SHAREDSECRET);
                    byte[] messageDataOut = xmlOut.GetBytes();

                    // discovered result
                    if (!output.ValidCredentials)
                        return new DiscoverApplicationResponse(DiscoverApplicationReturnType.InvalidCredentials, "0",
                                                                   new DiscoverApplicationResponseMessage(messageDataOut));

                    if (!string.IsNullOrEmpty(output.Guid))
                        return new DiscoverApplicationResponse(DiscoverApplicationReturnType.Found, "0", new DiscoverApplicationResponseMessage(messageDataOut));
                    else // not
                    {
                        return new DiscoverApplicationResponse(DiscoverApplicationReturnType.NotFound, "0",
                                                               new DiscoverApplicationResponseMessage(messageDataOut));
                    }

                }
                catch (ActivationDataAccessEngineException ex)
                {
                    throw new FaultException<ActivationEngineFault>(new ActivationEngineFault("Cannot discover, error occured"), ex.Message+", "+(ex.InnerException!=null?ex.InnerException.Message:"no inner message"), new FaultCode("db"));
                    //throw new ActivationEngineException("Cannot register, error occured", ex);
                }

            }
            catch (FaultException<ActivationEngineFault> ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new FaultException<ActivationEngineFault>(new ActivationEngineFault("Cannot discover, unknown processing error occured"), ex.Message + ", " + (ex.InnerException != null ? ex.InnerException.Message : "no inner message"), new FaultCode("unknown"));
            }
        }

        #endregion


        public UpdateApplicationRegistrationResponse UpdateApplicationRegistration(UpdateApplicationRegistrationRequest updateRequest)
        {
            throw new NotImplementedException();
        }


        
    }
}
