﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MindServer.Activation.Data
{
	/// <summary>
	/// Registration output with new registration info
	/// </summary>
	public class DiscoverOutput
	{
        public DiscoverOutput(string guid, string publicIp, string internalIp, 
            Int32 patientId, Int32 therapeutistId, string therapeutistName, bool validCredentials)
		{
			Guid = guid;
            PublicIpAddress = publicIp;
            InternalIpAddress = internalIp;
            ValidCredentials = validCredentials;
            LoggedPatientId = patientId;
            TherapeutistId = therapeutistId;
            TherapeutistLogin = therapeutistName;
		}

		/// <summary>
		/// Application guid
		/// </summary>
		public string Guid { get; protected set; }

        /// <summary>
        /// Address public
        /// </summary>
        public string PublicIpAddress { get; protected set; }

		/// <summary>
		/// Address internal
		/// </summary>
		public string InternalIpAddress { get; protected set; }

	    /// <summary>
	    /// Credentials
	    /// </summary>
	    public bool ValidCredentials { get; protected set; }

	    public Int32 LoggedPatientId { get; protected set; }
        public Int32 TherapeutistId { get; protected set; }
        public string TherapeutistLogin { get; protected set; }
	}
}
