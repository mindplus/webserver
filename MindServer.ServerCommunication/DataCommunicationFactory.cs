﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using Data;
using MindGateEEG.Communication.WcfCommunication.Management.Contracts;
using MindServer.ServerCommunication.Data;
using MindServer.ServerCommunication.Exceptions;
using MindServer.SqlServer.Model;
using MindServer.SqlServer.Model.Data;

namespace MindServer.ServerCommunication
{
    /// <summary>
    /// Data processing Entity Access
    /// </summary>
    public class DataCommunicationFactory
    {
	    private string _connectionString;
		public DataCommunicationFactory(string connectionString)
		{
			_connectionString = connectionString;
		}

        public Person GetPerson(string login)
        {
            try
            {
                using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
                {
                    MindPerson modelPerson = dbContext.Persons.FirstOrDefault(a => a.Login.Equals(login));

                    if (modelPerson != null)
                    {
                        Person person = new Person();
                        person.Login = modelPerson.Login;
                        return person;
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public List<Person> GetPatients(string guid)
        {
            List<Person> persons = new List<Person>();

            try
            {
                using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
                {
                    foreach(MindPerson modelPerson in dbContext.Persons)
                    {
                        Person person = new Person();
                        person.Id =modelPerson.Id;
                        person.Type = modelPerson.Type;
                        person.Login = modelPerson.Login;
                        person.Name = modelPerson.Name;
                        person.Surname = modelPerson.Surname;
                        person.Middlename = modelPerson.Middlename;
                        person.Email = modelPerson.Email;
                        person.DateOfBirth=modelPerson.DateOfBirth;
                        person.Gender = modelPerson.Gender;
                        person.Pin = modelPerson.Pin;
                        person.CountryCode = modelPerson.CountryCode;
                        person.Address = modelPerson.Address;
                        person.City=modelPerson.City;
                        person.Zip = modelPerson.Zip;
                        person.Phone = modelPerson.Phone;
                        person.PhysicianSurname = modelPerson.PhysicianSurname;
                        person.PhysicianLogin = modelPerson.PhysicianLogin;
                        person.Diagnosis = modelPerson.Diagnosis;
                        person.Medication=modelPerson.Medication;
                        person.OtherTherapy=modelPerson.OtherTherapy;

                        persons.Add(person);
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return persons;

        }

        public Person GetPatient(string guid, Int32 id)
        {
            try
            {
                using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
                {
                    MindPerson modelPerson = dbContext.Persons.Find(id);
                    if (modelPerson!=null)
                    {
                        Person person = new Person();
                        person.Id = modelPerson.Id;
                        person.Type = modelPerson.Type;
                        person.Login = modelPerson.Login;
                        person.Name = modelPerson.Name;
                        person.Surname = modelPerson.Surname;
                        person.Middlename = modelPerson.Middlename;
                        person.Email = modelPerson.Email;
                        person.DateOfBirth = modelPerson.DateOfBirth;
                        person.Gender = modelPerson.Gender;
                        person.Pin = modelPerson.Pin;
                        person.CountryCode = modelPerson.CountryCode;
                        person.Address = modelPerson.Address;
                        person.City = modelPerson.City;
                        person.Zip = modelPerson.Zip;
                        person.Phone = modelPerson.Phone;
                        person.PhysicianSurname = modelPerson.PhysicianSurname;
                        person.PhysicianLogin = modelPerson.PhysicianLogin;
                        person.Diagnosis = modelPerson.Diagnosis;
                        person.Medication = modelPerson.Medication;
                        person.OtherTherapy = modelPerson.OtherTherapy;

                        return person;
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return new Person();

        }

        public Person GetPersonByLogin(string login, string password)
        {
            try
            {
                using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
                {
                    MindPerson modelPerson = dbContext.Persons.FirstOrDefault(a=>a.Login.Equals(login) & a.Password.Equals(password));
                    if (modelPerson != null)
                    {
                        Person person = new Person();
                        person.Id = modelPerson.Id;
                        person.Type = modelPerson.Type;
                        person.Login = modelPerson.Login;
                        person.Name = modelPerson.Name;
                        person.Surname = modelPerson.Surname;
                        person.Middlename = modelPerson.Middlename;
                        person.Email = modelPerson.Email;
                        person.DateOfBirth = modelPerson.DateOfBirth;
                        person.Gender = modelPerson.Gender;
                        person.Pin = modelPerson.Pin;
                        person.CountryCode = modelPerson.CountryCode;
                        person.Address = modelPerson.Address;
                        person.City = modelPerson.City;
                        person.Zip = modelPerson.Zip;
                        person.Phone = modelPerson.Phone;
                        person.PhysicianSurname = modelPerson.PhysicianSurname;
                        person.PhysicianLogin = modelPerson.PhysicianLogin;
                        person.Diagnosis = modelPerson.Diagnosis;
                        person.Medication = modelPerson.Medication;
                        person.OtherTherapy = modelPerson.OtherTherapy;

                        return person;
                    }
                }
            }
            catch (DbEntityValidationException ex)
            {
                string errors = "";
                foreach (DbEntityValidationResult result in ex.EntityValidationErrors)
                {
                    foreach (DbValidationError re in result.ValidationErrors)
                    {
                        errors += re.ErrorMessage + " (" + re.PropertyName + ") ";
                    }
                }
                throw new ServerCommunicationDataAccessEngineException("Cannot find user " + ex.Message + (ex.InnerException != null ? ex.InnerException.Message : "") + errors, ex);
            }
            catch (Exception ex)
            {
                throw new ServerCommunicationDataAccessEngineException("Cannot find user " + ex.Message + (ex.InnerException != null ? ex.InnerException.Message : ""), ex);
            }
            return new Person();

        }

        /// <summary>
        /// Add new person
        /// </summary>
        /// <param name="person"></param>
        /// <param name="password"></param>
        /// <param name="applicationGuid"></param>
        /// <param name="defaultApplication"></param>
        /// <returns></returns>
        public PersonManageOutput AddPerson(Person person, string password, string applicationGuid, bool defaultApplication)
        {
            try
            {
                using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
                {
                    using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                    {
                        // TODO: check user data too
                        // check if user exists
                        MindPerson modelPerson = dbContext.Persons.FirstOrDefault(a => a.Login.Equals(person.Login));

                        if (modelPerson != null)
                        {
                            return new PersonManageOutput(PersonManageResultType.LoginExists, modelPerson.Id);
                        }

                        modelPerson = new MindPerson(-1, person.Type, DateTime.Now, person.Login, person.Name,
                                                     person.Surname, password, person.Middlename,
                                                     person.Email, person.DateOfBirth, person.Gender,
                                                     person.Pin, person.CountryCode, person.Address,
                                                     person.City, person.Zip, person.Phone,
                                                     person.PhysicianSurname, person.PhysicianLogin,
                                                     person.Diagnosis, person.Medication, person.OtherTherapy);

                        dbContext.Persons.Add(modelPerson);
                        dbContext.SaveChanges();

                        if (!String.IsNullOrEmpty(applicationGuid))
                        {
                            MindRegisteredApplication modelRegisteredApplication;

                            using (ActivationDbContext dbContextA = new ActivationDbContext(_connectionString))
                            {
                                modelRegisteredApplication =
                                    dbContextA.RegisteredApplications.FirstOrDefault(a => a.Guid.Equals(applicationGuid));
                            }

                            if (modelRegisteredApplication != null)
                            {
                                dbContext.PersonApplications.Add(new MindPersonApplication(-1, modelPerson.Id,
                                                                                           modelRegisteredApplication.Id,
                                                                                           defaultApplication));
                                dbContext.SaveChanges();

                                scope.Complete();

                                return new PersonManageOutput(PersonManageResultType.Added, modelPerson.Id);
                            }
                            else
                            {
                                return new PersonManageOutput(PersonManageResultType.ApplicationNotFound, null);
                            }
                        }
                        scope.Complete();
                        return new PersonManageOutput(PersonManageResultType.Added, null);
                    }
                }
            }
                catch(DbEntityValidationException ex )
                {
                    string errors = "";
                    foreach (DbEntityValidationResult result in ex.EntityValidationErrors)
                    {
                        foreach(DbValidationError re in result.ValidationErrors)
                        {
                            errors += re.ErrorMessage + " (" + re.PropertyName + ") ";
                        }
                    }
                    throw new ServerCommunicationDataAccessEngineException("Cannot add user " + ex.Message + (ex.InnerException != null ? ex.InnerException.Message : "") + errors, ex);
                }
            catch (Exception ex)
            {
                throw new ServerCommunicationDataAccessEngineException("Cannot add user "+ex.Message +(ex.InnerException!=null?ex.InnerException.Message:""), ex);
            }

        }

        /// <summary>
        /// Update person data
        /// </summary>
        /// <param name="person"></param>
        /// <param name="password"></param>
        /// <param name="applicationGuid"></param>
        /// <param name="defaultApplication"></param>
        /// <returns></returns>
        public PersonManageOutput UpdatePerson(Person person, string password, string applicationGuid, bool defaultApplication)
        {
            try
            {
                using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
                {
                    using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                    {
                        // TODO: check user data too
                        // check if user exists
                        MindPerson modelPerson = dbContext.Persons.FirstOrDefault(a => a.Id==person.Id);

                        if (modelPerson == null)
                        {
                            return new PersonManageOutput(PersonManageResultType.DataNotFound, modelPerson.Id);
                        }

                        modelPerson.Login=person.Login;
                        modelPerson.Name = person.Name;
                        modelPerson.Surname = person.Surname;

                        if (!String.IsNullOrEmpty(password))
                            modelPerson.Password = password;

                        dbContext.SaveChanges();

                        if (!String.IsNullOrEmpty(applicationGuid))
                        {
                            MindRegisteredApplication modelRegisteredApplication;

                            using (ActivationDbContext dbContextA = new ActivationDbContext(_connectionString))
                            {
                                modelRegisteredApplication =
                                    dbContextA.RegisteredApplications.FirstOrDefault(a => a.Guid.Equals(applicationGuid));
                            }

                            if (modelRegisteredApplication != null)
                            {
                                dbContext.PersonApplications.Add(new MindPersonApplication(-1, modelPerson.Id,
                                                                                           modelRegisteredApplication.Id,
                                                                                           defaultApplication));
                                dbContext.SaveChanges();

                                scope.Complete();

                                return new PersonManageOutput(PersonManageResultType.Updated, modelPerson.Id);
                            }
                            else
                            {
                                return new PersonManageOutput(PersonManageResultType.ApplicationNotFound, null);
                            }
                        }
                        scope.Complete();
                        return new PersonManageOutput(PersonManageResultType.Updated, null);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ServerCommunicationDataAccessEngineException("Cannot update user", ex);
            }
        }

        /// <summary>
        /// Remove person
        /// </summary>
        /// <param name="person"></param>
        /// <returns></returns>
        public PersonManageOutput RemovePerson(Person person)
        {
            try
            {
                using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
                {
                    using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                    {
                        // TODO: check user data too
                        // check if user exists
                        MindPerson modelPerson = dbContext.Persons.FirstOrDefault(a => a.Id == person.Id);

                        if (modelPerson == null)
                        {
                            return new PersonManageOutput(PersonManageResultType.DataNotFound, modelPerson.Id);
                        }


                        List<MindPersonApplication> apps = dbContext.PersonApplications.Where(a => a.PersonId == person.Id).ToList();

                        foreach (MindPersonApplication mindPersonApplication in apps)
                        {
                            dbContext.PersonApplications.Remove(mindPersonApplication);
                        }
                        dbContext.SaveChanges();

                        
                        scope.Complete();
                        return new PersonManageOutput(PersonManageResultType.Removed, null);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ServerCommunicationDataAccessEngineException("Cannot remove user", ex);
            }

        }

        public System.Collections.Generic.List<ComSession> GetSessions(string guid)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
            {
                List<ComSession> sessions = new List<ComSession>();
                foreach (DbSession modelSession in dbContext.Sessions.Where(a=>a.Guid.Equals(guid)))
                {
                    sessions.Add(new ComSession(modelSession.Id,
                        modelSession.PatientId, modelSession.CurrentRoundIndex, modelSession.StartTimeDate,
                        modelSession.RoundsCount, modelSession.BandSetId, modelSession.MethodName,
                        modelSession.RewardBand, modelSession.Inhibit1Band, modelSession.Inhibit2Band,
                        modelSession.AwardDelay, modelSession.AutoNextRound, modelSession.LogFileName,
                        modelSession.SessionNumber));
                }

                return sessions;
            }
        }

        public System.Collections.Generic.List<ComSession> GetSessionsForPatient(string guid, int patientId)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
            {
                List<ComSession> sessions = new List<ComSession>();
                foreach (DbSession modelSession in dbContext.Sessions.Where(a=>a.PatientId == patientId & a.Guid.Equals(guid)))
                {
                    sessions.Add(new ComSession(modelSession.Id,
                        modelSession.PatientId, modelSession.CurrentRoundIndex, modelSession.StartTimeDate,
                        modelSession.RoundsCount, modelSession.BandSetId, modelSession.MethodName,
                        modelSession.RewardBand, modelSession.Inhibit1Band, modelSession.Inhibit2Band,
                        modelSession.AwardDelay, modelSession.AutoNextRound, modelSession.LogFileName,
                        modelSession.SessionNumber));
                }

                return sessions;
            }
        }

        public ComSession GetLastSession(string guid, int patientId)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
            {
                DbSession modelSession = dbContext.Sessions.Where(p => p.PatientId == patientId & p.Guid.Equals(guid)).OrderByDescending(t => t.StartTimeDate).First();

                if (modelSession != null)
                {
                    return new ComSession(modelSession.Id,
                                       modelSession.PatientId, modelSession.CurrentRoundIndex,
                                       modelSession.StartTimeDate,
                                       modelSession.RoundsCount, modelSession.BandSetId, modelSession.MethodName,
                                       modelSession.RewardBand, modelSession.Inhibit1Band, modelSession.Inhibit2Band,
                                       modelSession.AwardDelay, modelSession.AutoNextRound, modelSession.LogFileName,
                                       modelSession.SessionNumber);
                }

                return null;
            }
        }

        public int AddSession(string guid, ComSession session)
        {
            session.SessionNumber = GetSessionsForPatient(guid, session.PatientId).Count() + 1; // TODO: wrong way to increment - to correct in future

            using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
            {
                DbSession modelSession = new DbSession(
                    session.Id, guid, session.PatientId, session.CurrentRoundIndex, session.StartTimeDate, session.RoundsCount,
                    session.BandSetId, session.MethodName, session.RewardBand, session.Inhibit1Band, session.Inhibit2Band,
                    session.AwardDelay, session.AutoNextRound, session.LogFileName, session.SessionNumber);

                dbContext.Sessions.Add(modelSession);

                dbContext.SaveChanges();

                return modelSession.Id;
            }
        }

        public bool UpdateSession(string guid, ComSession session)
        {
            int currentRounds = this.GetSessionRounds(guid, session.Id).Count;
            
            using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
            {
                DbSession modelSession = dbContext.Sessions.Find(session.Id);

                if (modelSession != null)
                {
                    modelSession.CurrentRoundIndex = currentRounds;
                    modelSession.StartTimeDate = session.StartTimeDate;
                    modelSession.RoundsCount = session.RoundsCount;
                    modelSession.BandSetId = session.BandSetId;
                    modelSession.MethodName = session.MethodName;
                    modelSession.RewardBand = session.RewardBand;
                    modelSession.Inhibit1Band = session.Inhibit1Band;
                    modelSession.Inhibit2Band = session.Inhibit2Band;
                    modelSession.AwardDelay = session.AwardDelay;
                    modelSession.AutoNextRound = session.AutoNextRound;
                    modelSession.LogFileName = session.LogFileName;
                    modelSession.SessionNumber = session.SessionNumber;
                    dbContext.SaveChanges();
                    return true;
                }

                return false;
            }
        }

        public bool DeleteSession(string guid, int sessionId)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
            {
                DbSession modelSession = dbContext.Sessions.Find(sessionId);

                if (modelSession != null)
                {
                    dbContext.Sessions.Remove(modelSession);
                    dbContext.SaveChanges();
                    return true;
                }

                return false;
            }
        }

        public int AddRound(string guid, ComRound round)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
            {
                DbRound modelRound = new DbRound(
                   round.Id, round.RoundIndex, round.Session_ID, round.Game_ID, round.Flags, round.PlannedDuration,
                   round.TimeStart, round.TimeEnd, round.DataFileName, round.PercentAugment, round.PercentInhibit_1,
                   round.PercentInhibit_2, round.AvgAugmentThreshold, round.AvgInhibit_1_Threshold, round.AvgInhibit_2_Threshold,
                   round.AvgAlphaBandValue, round.AvgBetaBandValue, round.AvgBeta_2BandValue, round.AvgThetaBandValue,
                   round.AvgSMRBandValue, round.AvgDeltaBandValue, round.Points);

                dbContext.Rounds.Add(modelRound);

                dbContext.SaveChanges();

                return modelRound.Id;
            }
        }

        public bool UpdateRound(string guid, ComRound round)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
            {
                DbRound modelRound = dbContext.Rounds.Find(round.Id);

                if (modelRound != null)
                {
                    modelRound.RoundIndex = round.RoundIndex;
                    modelRound.Session_ID = round.Session_ID;
                    modelRound.Game_ID = round.Game_ID;
                    modelRound.Flags = round.Flags;
                    modelRound.PlannedDuration = round.PlannedDuration;
                    modelRound.TimeStart = round.TimeStart;
                    modelRound.TimeEnd = round.TimeEnd;
                    modelRound.DataFileName = round.DataFileName;
                    modelRound.PercentAugment = round.PercentAugment;
                    modelRound.PercentInhibit_1 = round.PercentInhibit_1;
                    modelRound.PercentInhibit_2 = round.PercentInhibit_2;
                    modelRound.AvgAugmentThreshold = round.AvgAugmentThreshold;
                    modelRound.AvgInhibit_1_Threshold = round.AvgInhibit_1_Threshold;
                    modelRound.AvgInhibit_2_Threshold = round.AvgInhibit_2_Threshold;
                    modelRound.AvgAlphaBandValue = round.AvgAlphaBandValue;
                    modelRound.AvgBetaBandValue = round.AvgBetaBandValue;
                    modelRound.AvgBeta_2BandValue = round.AvgBeta_2BandValue;
                    modelRound.AvgThetaBandValue = round.AvgThetaBandValue;
                    modelRound.AvgSMRBandValue = round.AvgSMRBandValue;
                    modelRound.AvgDeltaBandValue = round.AvgDeltaBandValue;
                    modelRound.Points = round.Points;

                    dbContext.SaveChanges();

                    return true;
                }

                return false;
            }
        }

        public bool DeleteRound(string guid, int roundId)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
            {
                DbRound modelRound = dbContext.Rounds.Find(roundId);

                if (modelRound != null)
                {
                    dbContext.Rounds.Remove(modelRound);
                    dbContext.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public System.Collections.Generic.List<ComRound> GetSessionRounds(string guid, int sessionId)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
            {
                List<ComRound> rounds = new List<ComRound>();

                foreach (DbRound dbRound in dbContext.Rounds.Where(s => s.Session_ID == sessionId))
                {
                    rounds.Add(
                        new ComRound(
                            dbRound.Id, dbRound.RoundIndex, dbRound.Session_ID, dbRound.Game_ID, dbRound.Flags, dbRound.PlannedDuration,
                    dbRound.TimeStart, dbRound.TimeEnd, dbRound.DataFileName, dbRound.PercentAugment, dbRound.PercentInhibit_1,
                    dbRound.PercentInhibit_2, dbRound.AvgAugmentThreshold, dbRound.AvgInhibit_1_Threshold, dbRound.AvgInhibit_2_Threshold,
                    dbRound.AvgAlphaBandValue, dbRound.AvgBetaBandValue, dbRound.AvgBeta_2BandValue, dbRound.AvgThetaBandValue,
                    dbRound.AvgSMRBandValue, dbRound.AvgDeltaBandValue, dbRound.Points
                            )
                        );
                }

                return rounds;
            }
        }

        public System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<int, int>> GetSessionRoundPoints(
            string guid, int sessionId)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
            {
                List<KeyValuePair<int, int>> rounds = new List<KeyValuePair<int, int>>();

                foreach (DbRound dbRound in dbContext.Rounds.Where(s => s.Session_ID == sessionId))
                {
                    rounds.Add(
                        new KeyValuePair<int, int>(dbRound.RoundIndex, dbRound.Points)
                        );
                }

                return rounds;
            }
        }

        public System.Collections.Generic.List<ComRound> GetPatientRounds(string guid, int patientId)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
            {
                List<DbRound> selectedRounds = (from round in dbContext.Rounds join session in dbContext.Sessions on round.Session_ID equals session.Id where session.PatientId == patientId & session.Guid.Equals(guid) select round).ToList<DbRound>();
                List<ComRound> result = new List<ComRound>();
                foreach (DbRound dbRound in selectedRounds)
                {
                    result.Add(new ComRound(dbRound.Id, dbRound.RoundIndex, dbRound.Session_ID, dbRound.Game_ID, dbRound.Flags, dbRound.PlannedDuration,
                    dbRound.TimeStart, dbRound.TimeEnd, dbRound.DataFileName, dbRound.PercentAugment, dbRound.PercentInhibit_1,
                    dbRound.PercentInhibit_2, dbRound.AvgAugmentThreshold, dbRound.AvgInhibit_1_Threshold, dbRound.AvgInhibit_2_Threshold,
                    dbRound.AvgAlphaBandValue, dbRound.AvgBetaBandValue, dbRound.AvgBeta_2BandValue, dbRound.AvgThetaBandValue,
                    dbRound.AvgSMRBandValue, dbRound.AvgDeltaBandValue, dbRound.Points));
                }

                return result;
            }
        }

        public System.Collections.Generic.List<ComNotes> GetSessionNotes(string guid, int sessionId)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
            {
                List<ComNotes> notes = new List<ComNotes>();
                foreach (DbNotes modelNotes in dbContext.Notes.Where(n => n.SessionId == sessionId))
                {
                    notes.Add(new ComNotes(
                        modelNotes.Id, modelNotes.SessionId, modelNotes.Note, modelNotes.CreateDate));
                }

                return notes;
            }
        }

        public int AddNote(string guid, ComNotes note)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
            {
                DbNotes modelNotes = new DbNotes(
                        note.Id, note.SessionId, note.Note, note.CreateDate);

                dbContext.Notes.Add(modelNotes);

                dbContext.SaveChanges();

                return modelNotes.Id;
            }
        }

        public bool UpdateNote(string guid, ComNotes note)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
            {
                DbNotes modelNotes = dbContext.Notes.Find(note.Id);

                if (modelNotes != null)
                {
                    modelNotes.SessionId = note.SessionId;
                    modelNotes.Note = note.Note;
                    modelNotes.CreateDate = note.CreateDate;
                    ;
                    dbContext.SaveChanges();

                    return true;
                }

                return false;
            }
        }

        public bool DeleteNote(string guid, int noteId)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
            {
                DbNotes modelNotes = dbContext.Notes.Find(noteId);

                if (modelNotes != null)
                {
                    dbContext.Notes.Remove(modelNotes);
                    dbContext.SaveChanges();

                    return true;
                }

                return false;
            }
        }



        public void UpdateApplicationTrace(string guid, Int32 userId, string internalIp, string publicIp)
        {
            using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
            {
                DbTrace newModelTrace = new DbTrace(-1, userId, internalIp, publicIp);
                DbTrace modelTrace = dbContext.Traces.FirstOrDefault(a => a.UserId == userId);

                if (modelTrace != null)
                    dbContext.Traces.Remove(modelTrace);

                dbContext.Traces.Add(newModelTrace);

                dbContext.SaveChanges();
            }
        }
    }
}
