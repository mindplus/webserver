﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CommunicationTest
{
    public partial class Serial : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string error = "";
            if (String.IsNullOrEmpty(ByteData.Text))
                error = "Byte data empty";
            
            int bytedata = -1;
            if (!int.TryParse(ByteData.Text, out bytedata))
                error = "Byte data invalid number";

            if (bytedata < 0 || bytedata > 255)
                error = "Byte data should be 0-255";

            if (String.IsNullOrEmpty(NumberOfKeys.Text))
                error = "NumberOfKeys data empty";

            int numkeys = -1;
            if (!int.TryParse(NumberOfKeys.Text, out numkeys))
                error = "Num keys data invalid number";

            if (numkeys < 0 || numkeys > 255)
                error = "Num keys should be 0-255";

            if (!string.IsNullOrEmpty(error))
            {
                LabelKeys.Text = "Invalid data:<br>" + error;
                return;
            }

            for(int i=0;i<numkeys;i++)
            {
                LabelKeys.Text += "<br>";
                LabelKeys.Text += GenerateSerial();
            }
        }

        protected string GenerateSerial()
        {
            try
            {
                byte apptype = byte.Parse(Type.SelectedValue);
                byte appsubtype = byte.Parse(Subtype.Text);
                byte bytedata = byte.Parse(ByteData.Text);
                byte keyforversion = byte.Parse(VersionType.SelectedValue);
                byte versionmajor = byte.Parse(Major.SelectedValue);
                byte versionminor = byte.Parse(Minor.SelectedValue);

                byte b = ((byte) (16 - apptype)); // 16-application type  (application type on 4 bits)

                byte bb = ((byte) ((byte) (appsubtype) << 4));
                b = (byte) (b + bb);

                byte v = versionmajor;

                if (keyforversion == 1) // if check minor and major set bit 8 (128) (so version is on 2 bits v.1, 2, 3)
                    v = (byte) (v | 128);

                //byte vv = ((byte)((byte)versionminor << 3));
                //v = (byte)(v + vv);

                byte[] bytes = new byte[]
                    {
                        ((byte) (255 - b)), ((byte) (255 - bytedata)),
                        ((byte) (255 - v)), ((byte) (255 - versionminor))
                    };
                string enc = BitConverter.ToString(bytes);

                string unique = GetUniqueKey();

                enc = enc.Replace("-", "");
                string checksumenc = CalculateChecksum(enc, 2);

                string key1 = String.Format("{0}{1}{2}{3}{4}",
                                            enc.Substring(0, 5),
                                            unique.Substring(0, 5),
                                            unique.Substring(5, 5),
                                            enc.Substring(5, 3) + checksumenc,
                                            unique.Substring(10)
                    );
                string key1checksum = CalculateChecksum(key1, 4);

                string key = String.Format("{0}-{1}-{2}-{3}-{4}",
                                           key1.Substring(0, 5),
                                           key1.Substring(5, 5),
                                           key1.Substring(10, 5),
                                           key1.Substring(15, 5),
                                           key1.Substring(20) + key1checksum);
                return key;
            }catch(Exception ex)
            {
                return "Error processing: " + ex.Message;
            }
        }

        private string CalculateChecksum(string dataToCalculate, int length)
        {
            byte[] byteToCalculate = Encoding.ASCII.GetBytes(
                dataToCalculate + "encryptedchecksumbasestring+==".ToUpper());
            int checksum = 0;
            foreach (byte chData in byteToCalculate)
            {
                checksum += chData;
            }
            if (length == 2)
            {
                checksum &= 0xff;
                return checksum.ToString("X2");
            }
            else
            {
                checksum &= 0xffff;
                return checksum.ToString("X4");
            }
        }

        private string GetUniqueKey()
        {
            int maxSize = 11;
            int minSize = 5;
            char[] chars = new char[62];
            string a;
            a = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            { result.Append(chars[b % (chars.Length - 1)]); }
            return result.ToString();
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            LabelDecodeGenerated.Text = "";
            string[] keys = LabelKeys.Text.Replace("<br>","|").Split('|');
            foreach (string key in keys)
            {
                if (!String.IsNullOrEmpty(key))
                {
                    LabelDecodeGenerated.Text += "<br>";
                    LabelDecodeGenerated.Text += DecodeKey(key);
                }
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            DecodedKey.Text = DecodeKey(Key.Text);
        }

        private string DecodeKey(string key)
        {
            try{
            string allkey = key.Replace("-", "");

            string k1 = allkey.Substring(0, 5);
            string k2 = allkey.Substring(5, 5);
            string k3 = allkey.Substring(10, 5);
            string k4 = allkey.Substring(15, 5);
            string k5 = allkey.Substring(20);

            // validate all checksum
            if (!CalculateChecksum(allkey.Substring(0,21),4).Equals(allkey.Substring(21)))
            {
                return key + " : invalid key [global checksum error]";
            }

            string data = k1 + k4.Substring(0, 3);

            if (!CalculateChecksum(data, 2).Equals(k4.Substring(3)))
            {
                return key + " : invalid key [data checksum error]";
            }

            byte data1 = (byte)(255-Convert.ToByte(data.Substring(0,2), 16) );
            byte data2 = (byte)(255-Convert.ToByte(data.Substring(2, 2), 16) );
            byte data3 = (byte)(255-Convert.ToByte(data.Substring(4, 2), 16) );
            byte data4 = (byte)(255-Convert.ToByte(data.Substring(6, 2), 16) );

            
            string applicationtype = ((16-data1) & 15).ToString();
            string applicationsubtype = (((data1)>>4) & 15).ToString();
            string datas = data2.ToString();
            string versioncheck = ((data3 & 128)==128).ToString();
            string versionmajor = (data3 & 7).ToString();
            string versionminor = (data4).ToString();

            return key + " : APPTYPE: " + applicationtype + ", APPSUBTYPE: " + applicationsubtype +
                   ", DATA: " + datas + ", VERSIONCHECK: " + versioncheck + ", MAJOR: " + versionmajor +
                   ", MINOR: " + versionminor;
            }
            catch (Exception ex)
            {
                return "Error processing: " + ex.Message;
            }
        }
    }
}