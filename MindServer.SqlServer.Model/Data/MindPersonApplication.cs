﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MindServer.SqlServer.Model.Data
{
	[Table("PersonApplications")]
	public class MindPersonApplication
	{
		public MindPersonApplication()
		{
			Id = -1;
		}

        public MindPersonApplication(int id, Int32 personId, Int32 applicationId, bool isMainApplication)
		{
			Id = id;
			PersonId = personId;
		    ApplicationId = applicationId;
            IsMainApplication = isMainApplication;
		}

		[Column("PersonApplicationId"), Required, Key, DatabaseGenerated(DatabaseGeneratedOption.Identity) ]
		public Int32 Id { get; protected set; }

		[Column("PersonId"), Required]
		public Int32 PersonId { get; protected set; }

        [Column("RegisteredApplicationId"), Required]
        public Int32 ApplicationId { get; set; }

        [Column("IsMainApplication"), Required]
        public bool IsMainApplication { get; set; }


        [ForeignKey("ApplicationId")]
        public MindRegisteredApplication RegisteredApplication { get; set; }

        [ForeignKey("PersonId")]
        public MindPerson Person { get; set; }

        
	}
}
