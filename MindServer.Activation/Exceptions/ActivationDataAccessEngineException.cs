﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommunicationEngine.Exceptions
{
    public class ActivationDataAccessEngineException : Exception
    {
        public Exception Exception { get; protected set; }
        public string Message { get; protected set; }
		public ActivationDataAccessEngineException(string message, Exception ex)
        {
            Exception = ex;
            Message = message;
        }
    }
}
