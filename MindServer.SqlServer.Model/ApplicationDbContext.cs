﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MindServer.SqlServer.Model.Data;

namespace MindServer.SqlServer.Model
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(string connectionString)
            : base(connectionString)
        {
            Configuration.LazyLoadingEnabled = false;
			Database.SetInitializer<ActivationDbContext>(null);//new CreateDatabaseIfNotExists<ActivationDbContext>());
        }

		public DbSet<MindPerson> Persons { get; set; }

        public DbSet<MindPersonApplication> PersonApplications { get; set; }

        /// <summary>
        /// Session records
        /// </summary>
        public DbSet<DbSession> Sessions { get; set; }

        /// <summary>
        /// Rounds records
        /// </summary>
        public DbSet<DbRound> Rounds { get; set; }

        /// <summary>
        /// Notes records
        /// </summary>
        public DbSet<DbNotes> Notes { get; set; }


        /// <summary>
        /// traces
        /// </summary>
        public DbSet<DbTrace> Traces { get; set; }
    }
}
