﻿using System;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Threading;
using System.Xml.Linq;
using CommunicationEngine;
using CommunicationEngine.Utils;
using Data;
using Data.Helpers;
using MindGateEEG.Communication.WcfCommunication.Management;
using MindGateEEG.Communication.WcfCommunication.Management.Contracts;
using MindGateEEG.Communication.WcfCommunication.Management.Exceptions;
using MindGateEEG.Communication.WcfCommunication.Management.Messages;
using MindGateEEG.Communication.WcfCommunication.Management.Serialization;
using MindServer.ServerCommunication.Data;
using MindServer.ServerCommunication.Exceptions;

namespace MindServer.ServerCommunication
{
    /// <summary>
    /// data management host service
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class DataCommunicationServiceHost : IDataCommunicationService
    {
        #region properties
        private CommunicationServiceHost<IDataCommunicationService> _host;
        private readonly string _addressRoot;
        #endregion

        public DataCommunicationServiceHost(string root)
        {
            _addressRoot = root;
        }

        public DataCommunicationServiceHost()
        {
        }

        #region initialization
        /// <summary>
        /// call only in windows environment
        /// </summary>
        public void InitializeCommunication()
        {
            string endpointSuffix = Definitions.ServiceName;

            // winforms
            if (Environment.UserInteractive)
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback(
                                                 (obj) =>
                                                 {
                                                     _host =
                                                         new CommunicationServiceHost
                                                             <IDataCommunicationService>(_addressRoot,
                                                                                                    endpointSuffix, BindingType.WsHttp);
                                                     _host.Initialize(this);
                                                     _host.Open();
                                                 }));
            }
            else
            {
                _host = new CommunicationServiceHost<IDataCommunicationService>(_addressRoot, endpointSuffix, BindingType.WsHttp);
                _host.Initialize(this);
                _host.Open();
            }

        }

        public void Dispose()
        {

            if (_host != null)
            {
                _host.Close();
                _host = null;
            }

        }
        #endregion

        #region users
        public PersonResponse ManagePerson(PersonRequest request)
        {
            // todo: check permission
            try
            {
                // processing input block - decode request
                string xml = request.PersonRequestMessage.Data.GetString();
                xml = CryptoHelper.DecryptStringAES(xml, CryptoHelper.SHAREDSECRET);
                XDocument document = xml.DeserializeToXDocument();

                PersonRequestMessageContent requestContent =
                    document.DeserializeToPersonRequestMessageContent();

                
                // processing user block
                try
                {
                    string connectionString = ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                    DataCommunicationFactory factory = new DataCommunicationFactory(connectionString);

                    PersonManageOutput output;

                    if (request.RequestType == PersonActionType.Add)
                    {
                        if (factory.GetPerson(requestContent.Person.Login)!=null)
                        {
                            output = new PersonManageOutput(PersonManageResultType.LoginExists, -1);
                        }
                        else
                        {
                            output = factory.AddPerson(requestContent.Person, requestContent.Password,
                                                       requestContent.ApplicationGuid, true);
                        }
                    }
                    else if (request.RequestType == PersonActionType.Update)
                        output = factory.UpdatePerson(requestContent.Person, requestContent.Password, requestContent.ApplicationGuid, true);
                    else if (request.RequestType == PersonActionType.Remove)
                        output = factory.RemovePerson(requestContent.Person);
                    else throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot process, unknown command"), "Unknown request command", new FaultCode("request"));
                    
                    // processing out block, set registration data and encode response
                    PersonResponseMessageContent message = new PersonResponseMessageContent();
                    message.Id = output.UserId;

                    // TODO: result type
                    //output.ResultType == PersonManageResultType.LoginExists

                    XDocument documentOut = message.SerializeToDocument();
                    string xmlOut = documentOut.SerializeToXml();
                    xmlOut = CryptoHelper.EncryptStringAES(xmlOut, CryptoHelper.SHAREDSECRET);
                    byte[] messageDataOut = xmlOut.GetBytes();

                    return new PersonResponse(PersonActionReturnType.Processed, "0", new PersonResponseMessage(messageDataOut));
                }
                catch (ServerCommunicationDataAccessEngineException ex)
                {
                    throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot register, error occured"), ex.Message, new FaultCode("db"));
                }

            }
            catch (FaultException<DataCommunicationEngineFault> ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot register, unknown processing error occured"), ex.Message, new FaultCode("unknown"));
            }
        }


        public System.Collections.Generic.List<Person> GetPatients(string guid)
        {
            try
            {
                string connectionString =
                    ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                DataCommunicationFactory factory = new DataCommunicationFactory(connectionString);
                return factory.GetPatients(guid);
            }catch(Exception ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot register, unknown processing error occured"), ex.Message, new FaultCode("unknown"));
            }
        }

        public Person GetPatient(string guid, Int32 id)
        {
            try
            {
                string connectionString =
                    ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                DataCommunicationFactory factory = new DataCommunicationFactory(connectionString);
                return factory.GetPatient(guid, id);
            }
            catch (Exception ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot register, unknown processing error occured"), ex.Message, new FaultCode("unknown"));
            }
        }

        public Person GetPersonByLogin(string login, string password)
        {
            try
            {
                string connectionString =
                    ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                DataCommunicationFactory factory = new DataCommunicationFactory(connectionString);
                return factory.GetPersonByLogin(login,password);
            }
            catch (ServerCommunicationDataAccessEngineException ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot process, error occured"), ex.Message, new FaultCode("db"));
            }
            catch (Exception ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot process, unknown processing error occured"), ex.Message, new FaultCode("unknown"));
            }
        }
        #endregion






        public System.Collections.Generic.List<ComSession> GetSessions(string guid)
        {
            try
            {
                string connectionString =
                    ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                DataCommunicationFactory factory = new DataCommunicationFactory(connectionString);
                return factory.GetSessions(guid);
            }
            catch (Exception ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot get data, unknown processing error occured"), ex.Message, new FaultCode("unknown"));
            }
        }

        public System.Collections.Generic.List<ComSession> GetSessionsForPatient(string guid, int patientId)
        {
            try
            {
                string connectionString =
                    ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                DataCommunicationFactory factory = new DataCommunicationFactory(connectionString);
                return factory.GetSessionsForPatient(guid, patientId);
            }
            catch (Exception ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot get data, unknown processing error occured"), ex.Message, new FaultCode("unknown"));
            }
        }

        public ComSession GetLastSession(string guid, int patientId)
        {
            try
            {
                string connectionString =
                    ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                DataCommunicationFactory factory = new DataCommunicationFactory(connectionString);
                return factory.GetLastSession(guid, patientId);
            }
            catch (Exception ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot get data, unknown processing error occured"), ex.Message, new FaultCode("unknown"));
            }
        }

        public int AddSession(string guid, ComSession session)
        {
            try
            {
                string connectionString =
                    ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                DataCommunicationFactory factory = new DataCommunicationFactory(connectionString);
                return factory.AddSession(guid, session);
            }
            catch (Exception ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot get data, unknown processing error occured"), ex.Message, new FaultCode("unknown"));
            }
        }

        public bool UpdateSession(string guid, ComSession session)
        {
            try
            {
                string connectionString =
                    ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                DataCommunicationFactory factory = new DataCommunicationFactory(connectionString);
                return factory.UpdateSession(guid, session);
            }
            catch (Exception ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot get data, unknown processing error occured"), ex.Message, new FaultCode("unknown"));
            }
        }

        public bool DeleteSession(string guid, int sessionId)
        {
            try
            {
                string connectionString =
                    ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                DataCommunicationFactory factory = new DataCommunicationFactory(connectionString);
                return factory.DeleteSession(guid, sessionId);
            }
            catch (Exception ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot get data, unknown processing error occured"), ex.Message, new FaultCode("unknown"));
            }
        }

        public int AddRound(string guid, ComRound session)
        {
            try
            {
                string connectionString =
                    ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                DataCommunicationFactory factory = new DataCommunicationFactory(connectionString);
                return factory.AddRound(guid, session);
            }
            catch (Exception ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot get data, unknown processing error occured"), ex.Message, new FaultCode("unknown"));
            }
        }

        public bool UpdateRound(string guid, ComRound session)
        {
            try
            {
                string connectionString =
                    ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                DataCommunicationFactory factory = new DataCommunicationFactory(connectionString);
                return factory.UpdateRound(guid, session);
            }
            catch (Exception ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot get data, unknown processing error occured"), ex.Message, new FaultCode("unknown"));
            }
        }

        public bool DeleteRound(string guid, int roundId)
        {
            try
            {
                string connectionString =
                    ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                DataCommunicationFactory factory = new DataCommunicationFactory(connectionString);
                return factory.DeleteRound(guid, roundId);
            }
            catch (Exception ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot get data, unknown processing error occured"), ex.Message, new FaultCode("unknown"));
            }
        }

        public System.Collections.Generic.List<ComRound> GetSessionRounds(string guid, int sessionId)
        {
            try
            {
                string connectionString =
                    ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                DataCommunicationFactory factory = new DataCommunicationFactory(connectionString);
                return factory.GetSessionRounds(guid, sessionId);
            }
            catch (Exception ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot get data, unknown processing error occured"), ex.Message, new FaultCode("unknown"));
            }
        }

        public System.Collections.Generic.List<System.Collections.Generic.KeyValuePair<int, int>> GetSessionRoundPoints(string guid, int sessionId)
        {
            try
            {
                string connectionString =
                    ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                DataCommunicationFactory factory = new DataCommunicationFactory(connectionString);
                return factory.GetSessionRoundPoints(guid, sessionId);
            }
            catch (Exception ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot get data, unknown processing error occured"), ex.Message, new FaultCode("unknown"));
            }
        }

        public System.Collections.Generic.List<ComRound> GetPatientRounds(string guid, int patientId)
        {
            try
            {
                string connectionString =
                    ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                DataCommunicationFactory factory = new DataCommunicationFactory(connectionString);
                return factory.GetPatientRounds(guid, patientId);
            }
            catch (Exception ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot get data, unknown processing error occured"), ex.Message, new FaultCode("unknown"));
            }
        }

        public System.Collections.Generic.List<ComNotes> GetSessionNotes(string guid, int sessionId)
        {
            try
            {
                string connectionString =
                    ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                DataCommunicationFactory factory = new DataCommunicationFactory(connectionString);
                return factory.GetSessionNotes(guid, sessionId);
            }
            catch (Exception ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot get data, unknown processing error occured"), ex.Message, new FaultCode("unknown"));
            }
        }

        public int AddNote(string guid, ComNotes note)
        {
            try
            {
                string connectionString =
                    ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                DataCommunicationFactory factory = new DataCommunicationFactory(connectionString);
                return factory.AddNote(guid, note);
            }
            catch (Exception ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot get data, unknown processing error occured"), ex.Message, new FaultCode("unknown"));
            }
        }

        public bool UpdateNote(string guid, ComNotes note)
        {
            try
            {
                string connectionString =
                    ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                DataCommunicationFactory factory = new DataCommunicationFactory(connectionString);
                return factory.UpdateNote(guid, note);
            }
            catch (Exception ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot get data, unknown processing error occured"), ex.Message, new FaultCode("unknown"));
            }
        }

        public bool DeleteNote(string guid, int noteId)
        {
            try
            {
                string connectionString =
                    ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                DataCommunicationFactory factory = new DataCommunicationFactory(connectionString);
                return factory.DeleteNote(guid, noteId);
            }
            catch (Exception ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot get data, unknown processing error occured"), ex.Message, new FaultCode("unknown"));
            }
        }


        public void UpdateApplicationTrace(string endpoint)
        {
            try
            {
                try
                {
                    string guid;
                    ServiceHelper.GetClientGuid(out guid); // identify client
                    Int32 userId;
                    ServiceHelper.GetClientUserId(out userId); // identify client

                    string ip;
                    int port;
                    bool investigatedIp = ServiceHelper.RetrieveClientPublicIpAddress(out ip, out port);

                    string publicEndpoint = ip + ":" + port;

                    string connectionString = ConfigurationManager.ConnectionStrings["MindServerConnection"].ConnectionString;
                    DataCommunicationFactory factory = new DataCommunicationFactory(connectionString);
                    factory.UpdateApplicationTrace(guid, userId, endpoint, publicEndpoint);
                }
                catch (ServerCommunicationDataAccessEngineException ex)
                {
                    throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot discover, error occured"), ex.Message + ", " + (ex.InnerException != null ? ex.InnerException.Message : "no inner message"), new FaultCode("db"));
                    //throw new ActivationEngineException("Cannot register, error occured", ex);
                }

            }
            catch (FaultException<DataCommunicationEngineFault> ex)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new FaultException<DataCommunicationEngineFault>(new DataCommunicationEngineFault("Cannot discover, unknown processing error occured"), ex.Message + ", " + (ex.InnerException != null ? ex.InnerException.Message : "no inner message"), new FaultCode("unknown"));
            }
        }
    }
}
