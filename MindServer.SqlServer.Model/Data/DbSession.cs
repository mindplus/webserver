﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MindServer.SqlServer.Model.Data
{
    [Table("SessionsTable")]
    public class DbSession
    {
        public DbSession()
        {
            Id = -1;
        }

        // TODO: add rounds as foreign?
        public DbSession(int id, string guid, int patientId, int currentRoundIndex, DateTime start, int roundsCount,
            int bandSetId, string methodName, string rewardBand, string inhibit1Band, string inhibit2Band,
            int awardDelay, int autoNextRound, string logFilename, int sessionNumber)
        {
            Guid = guid;
            Id = id;
            PatientId = patientId;
            CurrentRoundIndex = currentRoundIndex;
            StartTimeDate = start;
            RoundsCount = roundsCount;
            BandSetId = bandSetId;
            MethodName = methodName;
            RewardBand = rewardBand;
            Inhibit1Band = inhibit1Band;
            Inhibit2Band = inhibit2Band;
            AwardDelay = awardDelay;
            AutoNextRound = autoNextRound;
            LogFileName = logFilename;
            SessionNumber = sessionNumber;
        }

        [Column("Guid")]
        public string Guid { get; set; }

        [Column("CurentRoundNo")]
        public int CurrentRoundIndex { get; set; }

        [Key]
        [Column("ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("Patient_ID")]
        public int PatientId { get; set; }

        [Column("StartTimeDate")]
        public DateTime StartTimeDate { get; set; }

        [Column("RoundsCount")]
        public int RoundsCount { get; set; }

        [Column("BandSet_ID")]
        public int BandSetId { get; set; }

        [Column("MethodName")]
        public string MethodName { get; set; }

        [Column("RewardBand")]
        public string RewardBand { get; set; }

        [Column("Inhibit1Band")]
        public string Inhibit1Band { get; set; }

        [Column("Inhibit2Band")]
        public string Inhibit2Band { get; set; }

        [Column("AwardDelay")]
        public int AwardDelay { get; set; }

        [Column("AutoNextRound")]
        public int AutoNextRound { get; set; }

        [Column("LogFileName")]
        public string LogFileName { get; set; }

        [Column("Comments")]
        public string Comments { get; set; }

        [Column("SessionNumber")]
        public int SessionNumber { get; set; }
    }
}
