﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Routing;
using CommunicationEngine;
using MindGateEEG.Communication.WcfCommunication.Activation;
using MindGateEEG.Communication.WcfCommunication.Management;
using MindServer.Activation;
using MindServer.ServerCommunication;
using Definitions = MindGateEEG.Communication.WcfCommunication.Management.Definitions;

namespace WebServerApplication
{
    public class Global : HttpApplication
    {
        public class ServiceFactory
        {

            /// <summary>
            /// Register custom routes
            /// </summary>
            /// <param name="routes"></param>
            public static void RegisterRoutes(RouteCollection routes)
            {
                // Register services
                routes.Add(new ServiceRoute(MindGateEEG.Communication.WcfCommunication.Activation.Definitions.ServiceName,
                           new ApplicationActivationServiceHostFactory(), typeof(IApplicationActivation)));
                routes.Add(new ServiceRoute(Definitions.ServiceName,
                           new DataCommunicationServiceHostFactory(), typeof(IDataCommunicationService)));
            }

            public class ApplicationActivationServiceHostFactory : ServiceHostFactory
            {
                private CommunicationServiceHost<IApplicationActivation> _host;
                protected override ServiceHost CreateServiceHost(Type serviceType,
                 Uri[] baseAddresses)
                {
                    if (_host == null)
                    {
                        
                        _host =
                            new CommunicationServiceHost<IApplicationActivation>(
                                //"http://localhost:31150/", 
                                //"http://mindplus1.hostingasp.pl.hostingasp.pl/", 
                                System.Configuration.ConfigurationManager.AppSettings["serviceAddress"],
                                //baseAddresses[0].AbsoluteUri.Replace(Definitions.ServiceName,""),
                                MindGateEEG.Communication.WcfCommunication.Activation.Definitions.ServiceName, 
                                BindingType.WsHttp);
                        _host.Initialize(new ApplicationActivationServiceHost());
                    }
                    return _host.Host;
                }
            }

            public class DataCommunicationServiceHostFactory : ServiceHostFactory
            {
                private CommunicationServiceHost<IDataCommunicationService> _host;
                protected override ServiceHost CreateServiceHost(Type serviceType,
                 Uri[] baseAddresses)
                {
                    if (_host == null)
                    {
                        _host =
                            new CommunicationServiceHost<IDataCommunicationService>(
                                //"http://localhost:31150/",
                                //"http://mindplus1.hostingasp.pl.hostingasp.pl/",
                                System.Configuration.ConfigurationManager.AppSettings["serviceAddress"],
                                //baseAddresses[0].AbsoluteUri.Replace(Definitions.ServiceName, ""),
                                Definitions.ServiceName, BindingType.WsHttp);
                        _host.Initialize(new DataCommunicationServiceHost());
                    }
                    return _host.Host;
                }
            }
        }
        void Application_Start(object sender, EventArgs e)
        {
            ServiceFactory.RegisterRoutes(RouteTable.Routes);
        }

        

        

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown
        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }
    }
}
