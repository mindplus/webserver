﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MindServer.Activation.Data
{
	/// <summary>
	/// Registration output with new registration info
	/// </summary>
	public class RegistrationOutput
	{
		public RegistrationOutput(string guid, bool isNewRegistration, bool isActivationNeeded)
		{
			Guid = guid;
			IsNewRegistration = isNewRegistration;
            IsActivationNeeded = isActivationNeeded;
		}

		/// <summary>
		/// Application guid
		/// </summary>
		public string Guid { get; protected set; }

		/// <summary>
		/// Gets info about new registration or already registered
		/// </summary>
		public bool IsNewRegistration { get; protected set; }

		/// <summary>
		/// Gets info about activation
		/// </summary>
		public bool IsActivationNeeded { get; protected set; }
	}
}
