﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MindServer.SqlServer.Model.Data
{
    [Table("RoundsTable")]
    public class DbRound
    {
        public DbRound()
        {
            Id = -1;
        }

        public DbRound(int id, int roundIndex, int sessionId, int gameId, int flags, int plannedDuration,
            DateTime timeStart, DateTime timeEnd, string dataFileName, int percentAugment,
            int percentInhibit_1, int percentInhibit_2, double avgAugmentThreshold,
            double avgInhibit_1_Threshold, double avgInhibit_2_Threshold, double avgAlphaBandValue,
            double avgBetaBandValue, double avgBeta_2BandValue, double avgThetaBandValue,
            double avgSMRBandValue, double avgDeltaBandValue, int points)
        {
            Id = id;
            RoundIndex = roundIndex;
            Session_ID = sessionId;
            Game_ID = gameId;
            Flags = flags;
            PlannedDuration = plannedDuration;
            TimeEnd = timeEnd;
            TimeStart = timeStart;
            DataFileName = dataFileName;
            PercentAugment = percentAugment;
            PercentInhibit_1 = percentInhibit_1;
            PercentInhibit_2 = percentInhibit_2;
            AvgAugmentThreshold = avgAugmentThreshold;
            AvgInhibit_1_Threshold = avgInhibit_1_Threshold;
            AvgInhibit_2_Threshold = avgInhibit_2_Threshold;
            AvgAlphaBandValue = avgAlphaBandValue;
            AvgBetaBandValue = avgBetaBandValue;
            AvgBeta_2BandValue = avgBeta_2BandValue;
            AvgThetaBandValue = avgThetaBandValue;
            AvgSMRBandValue = avgSMRBandValue;
            AvgDeltaBandValue = avgDeltaBandValue;
            Points = points;
        }

        [Key]
        [Column("ID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("RoundNo")]
        public int RoundIndex { get; set; }

        [Column("Session_ID")]
        public int Session_ID { get; set; }

        [Column("Game_ID")]
        public int Game_ID { get; set; }

        [Column("Flags")]
        public int Flags { get; set; }

        [Column("PlannedDuration")]
        public int PlannedDuration { get; set; }

        [Column("TimeStart")]
        public DateTime TimeStart { get; set; }

        [Column("TimeEnd")]
        public DateTime TimeEnd { get; set; }

        [Column("DataFileName")]
        public string DataFileName { get; set; }

        [Column("PercentAugment")]
        public int PercentAugment { get; set; }

        [Column("PercentInhibit_1")]
        public int PercentInhibit_1 { get; set; }

        [Column("PercentInhibit_2")]
        public int PercentInhibit_2 { get; set; }

        [Column("AvgAugmentThreshold")]
        public double AvgAugmentThreshold { get; set; }

        [Column("AvgInhibit_1_Threshold")]
        public double AvgInhibit_1_Threshold { get; set; }

        [Column("AvgInhibit_2_Threshold")]
        public double AvgInhibit_2_Threshold { get; set; }

        [Column("AvgAlphaBandValue")]
        public double AvgAlphaBandValue { get; set; }

        [Column("AvgBetaBandValue")]
        public double AvgBetaBandValue { get; set; }

        [Column("AvgBeta_2BandValue")]
        public double AvgBeta_2BandValue { get; set; }

        [Column("AvgThetaBandValue")]
        public double AvgThetaBandValue { get; set; }

        [Column("AvgSMRBandValue")]
        public double AvgSMRBandValue { get; set; }

        [Column("AvgDeltaBandValue")]
        public double AvgDeltaBandValue { get; set; }

        [Column("Points")]
        public int Points { get; set; }

    }
}
