﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using CommunicationEngine.Exceptions;
using MindServer.Activation.Data;
using MindServer.SqlServer.Model;
using MindServer.SqlServer.Model.Data;

namespace MindServer.Activation
{
    public class ActivationFactory
    {
        private string _connectionString;

        public ActivationFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public bool IsApplicationRegisteredAndActivated(string guid)
        {
            try
            {
                using (ActivationDbContext dbContext = new ActivationDbContext(_connectionString))
                {
                    // check if application is already registered and activated
                    MindRegisteredApplication modelAlreadyRegisteredApplication =
                        dbContext.RegisteredApplications.FirstOrDefault(a => a.Guid.Equals(guid) & !a.IsActivationNeeded);

                    if (modelAlreadyRegisteredApplication != null)
                        return true;
                }
            }
            catch (Exception ex)
            {
                throw new ActivationDataAccessEngineException("Cannot check activation", ex);
            }

            return false;
        }

        /// <summary>
        /// Activate new application
        /// </summary>
        /// <returns>guid</returns>
        public ActivationOutput ActivateApplication(string guid, string serial)
        {
            try
            {
                using (ActivationDbContext dbContext = new ActivationDbContext(_connectionString))
                {
                    using (TransactionScope scope = 
                        new TransactionScope(TransactionScopeOption.Required,
                                            new TransactionOptions() { IsolationLevel = IsolationLevel.RepeatableRead })
                                            )
                    {
                        // check if application is already registered
                        MindRegisteredApplication modelAlreadyRegisteredApplication =
                            dbContext.RegisteredApplications.FirstOrDefault(a => a.Guid.Equals(guid));

                        if (modelAlreadyRegisteredApplication == null)
                        {
                            // application not registered, there is nothing to activate
                            return new ActivationOutput(guid, false, true, false, false, true);
                        }

                        // todo: validate serial
                        bool keyValid = true;
                        
                        // check count of activations with this serial
                        // https://mind-plus.atlassian.net/browse/MPDOC-6
                        // 2 installations on diff machines, multiple on one machine
                        int keyUsageCount = 
                            dbContext.SerialUsages.Where(
                                a =>
                                a.Serial.Equals(serial) &
                                a.RegisteredApplicationId != modelAlreadyRegisteredApplication.Id).Sum(
                                    a => (int?)a.ActivationCount)??0;
                        

                        
                        bool keyBlacklisted = false;

                        // if ok
                        if (keyUsageCount < 3)
                        {
                            MindSerialBlacklisted modelBlacklisted =
                                dbContext.BlacklistedSerials.FirstOrDefault(a => a.Serial.Equals(serial));
                            
                            if (modelBlacklisted == null)
                            {
                                // check if application has record
                                MindSerialUsage modelUsageExist =
                                    dbContext.SerialUsages.FirstOrDefault(
                                        a => a.RegisteredApplicationId == modelAlreadyRegisteredApplication.Id);
                                
                                if (modelUsageExist == null)
                                {
                                    MindSerialUsage modelUsage = new MindSerialUsage(-1,
                                                                                     modelAlreadyRegisteredApplication.
                                                                                         Id, serial, 1);
                                    dbContext.SerialUsages.Add(modelUsage);
                                    dbContext.SaveChanges();
                                }
                                else
                                {
                                    modelUsageExist.ActivationCount = modelUsageExist.ActivationCount + 1;
                                    dbContext.SaveChanges();
                                }
                                
                                // activate
                                modelAlreadyRegisteredApplication.IsActivationNeeded = false;
                                modelAlreadyRegisteredApplication.ActivationDate = DateTime.Now;
                                dbContext.SaveChanges();

                                scope.Complete();
                            }
                            else
                            {
                                keyBlacklisted = true;
                                modelAlreadyRegisteredApplication.IsActivationNeeded = true;
                                dbContext.SaveChanges();
                                scope.Complete();
                            }
                        }
                        else
                        {
                            modelAlreadyRegisteredApplication.IsActivationNeeded = true;
                            dbContext.SaveChanges();
                            scope.Complete();
                        }

                        // return new record info
                        ActivationOutput output = new ActivationOutput(modelAlreadyRegisteredApplication.Guid, true,
                                                                       modelAlreadyRegisteredApplication.IsActivationNeeded, 
                                                                       keyUsageCount>5, keyBlacklisted,
                                                                       keyValid);
                        return output;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ActivationDataAccessEngineException("Cannot activate application ", ex);
            }
        }

        public List<DbTherapeutist> GetApplicationUsers(string guid)
        {
            List<DbTherapeutist> list = new List<DbTherapeutist>();
            try
            {
                using (ApplicationDbContext dbContext = new ApplicationDbContext(_connectionString))
                {
                    string sql =
                            @"select persons.id, persons.login, persons.password from personapplications, persons, registeredapplications
where personapplications.PersonId = persons.PersonId and 
RegisteredApplications.RegisteredApplicationId = PersonApplications.RegisteredApplicationId and registeredapplications.Guid =@guid";

                    foreach (
                        DbTherapeutist app in dbContext.Database.SqlQuery<DbTherapeutist>(sql,
                                                                                                                new object
                                                                                                                    [
                                                                                                                    ]
                                                                                                                        {
                                                                                                                            new SqlParameter
                                                                                                                        (
                                                                                                                        "guid",
                                                                                                                        guid)
                                                                                                                        })
                        )
                    {
                        if (list.Find(a => a.Equals(app)) == null)
                            list.Add(app);
                    }
                }
            }
            catch(Exception ex)
            {
                
            }

            return list;
        }

        /// <summary>
        /// Register new application
        /// </summary>
        /// <returns>guid</returns>
        public RegistrationOutput RegisterApplication(DateTime registrationDate, string internalIpAddress,
                                                      string publicIpAddress, string applicationType,
                                                      string hardwareData)
        {
            try
            {
                using (ActivationDbContext dbContext = new ActivationDbContext(_connectionString))
                {
                    using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                    {
                        // check if application is already registered
                        MindRegisteredApplication modelAlreadyRegisteredApplication =
                            dbContext.RegisteredApplications.FirstOrDefault(
                                a => a.ApplicationType.Equals(applicationType) &
                                     a.HardwareData.Equals(hardwareData));
                        if (modelAlreadyRegisteredApplication != null)
                        {
                            
                            // is already registered - check hardware data and return registered info if match
                            // else update registration info
                            //if (!hardwareData.Equals(modelAlreadyRegisteredApplication.HardwareData))
                            {
                                // update hardware data and mark to reactivation
                                //modelAlreadyRegisteredApplication.HardwareData = hardwareData;
                                modelAlreadyRegisteredApplication.InternalIpAddress = internalIpAddress;
                                modelAlreadyRegisteredApplication.PublicIpAddress = publicIpAddress;
                                
                                dbContext.SaveChanges();

                            }
                             
                            return new RegistrationOutput(modelAlreadyRegisteredApplication.Guid, false,
                                                          modelAlreadyRegisteredApplication.IsActivationNeeded);
                             
                        }

                        // register new

                        // generate guid and verify it doesn't exists
                        string guid = string.Empty;

                        while (String.IsNullOrEmpty(guid))
                        {
                            guid = Guid.NewGuid().ToString();

                            MindRegisteredApplication verificationModel =
                                dbContext.RegisteredApplications.FirstOrDefault(a => a.Guid.Equals(guid));

                            if (verificationModel != null)
                                guid = string.Empty;
                        }

                        // create new record and save
                        MindRegisteredApplication modelRegisteredApplication =
                            new MindRegisteredApplication(-1, guid, registrationDate, applicationType, publicIpAddress,
                                                          internalIpAddress, hardwareData, true, null);

                        dbContext.RegisteredApplications.Add(modelRegisteredApplication);
                        dbContext.SaveChanges();
                        scope.Complete();

                        // return new record info
                        RegistrationOutput output = new RegistrationOutput(modelRegisteredApplication.Guid, true, true);
                        return output;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ActivationDataAccessEngineException("Cannot register application", ex);
            }
        }

        private class RegisteredApplication
        {
            public string Guid { get; set; }
            public string PublicIpAddress { get; set; }
            public string InternalIpAddress { get; set; }
        }
        /// <summary>
        /// Discover application
        /// </summary>
        /// <returns>guid</returns>
        public DiscoverOutput DiscoverApplication(string searcherGuid, string therapeutistName, string patientLogin, string patientPassword)
        {
            Int32 loggedPatientId = -1;
            Int32 therapeutistId = -1;
            try
            {
                using (ActivationDbContext dbContext = new ActivationDbContext(_connectionString))
                {
                    using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                    {

                        string lsql =
                            @"select top 1 PersonId from Persons where Password=@password and persons.Login =@login and type='P'";

                        List<Int32> patients = dbContext.Database.SqlQuery<Int32>(lsql, new object[]
                            {
                                new SqlParameter("login", patientLogin),
                                new SqlParameter("password", patientPassword)
                            }).ToList();

                        if (patients != null && patients.Count >0)
                            loggedPatientId = patients[0];

                        string tsql =
                            @"select top 1 PersonId from Persons where persons.Login =@login and type='T'";

                        List<Int32> therapeutist = dbContext.Database.SqlQuery<Int32>(tsql, new object[]
                            {
                                new SqlParameter("login", therapeutistName)
                            }).ToList();

                        if (therapeutist != null && therapeutist.Count >0)
                            therapeutistId = patients[0];

                        DbTrace trace = dbContext.Traces.FirstOrDefault(a => a.UserId == therapeutistId);

                        // find by trace
                        if (trace!=null)
                        {
                            DiscoverOutput output = new DiscoverOutput(
                                string.Empty, trace.PublicIp,
                                trace.InternalIp,
                                loggedPatientId, therapeutistId, therapeutistName,
                                loggedPatientId != -1);
                            return output;
                        }

                        string sql =
                            @"select registeredapplications.* from personapplications, persons, registeredapplications
where personapplications.PersonId = persons.PersonId and 
RegisteredApplications.RegisteredApplicationId = PersonApplications.RegisteredApplicationId and persons.Login =@login";

                        List<RegisteredApplication> applications = new List<RegisteredApplication>();
                        foreach (
                            RegisteredApplication app in dbContext.Database.SqlQuery<RegisteredApplication>(sql,
                                 new object[]{
                                      new SqlParameter("login",therapeutistName)})
                            )
                        {
                            if (applications.Find(a => a.Guid.Equals(app.Guid)) == null)
                                applications.Add(app);
                        }

                        if (applications.Count > 0)
                        {
                            DiscoverOutput output = new DiscoverOutput(
                                applications.Last().Guid, applications.Last().PublicIpAddress,
                                applications.Last().InternalIpAddress,
                                loggedPatientId, therapeutistId, therapeutistName,
                                loggedPatientId!=-1);
                            return output;
                        }
                        else
                        {
                            DiscoverOutput output = new DiscoverOutput(
                                string.Empty, string.Empty,
                                string.Empty,
                                loggedPatientId, therapeutistId, therapeutistName,
                                loggedPatientId != -1);
                            return output;
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                throw new ActivationDataAccessEngineException("Cannot register application", ex);
            }
        }
    }
}

