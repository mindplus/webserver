﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Serial.aspx.cs" Inherits="CommunicationTest.Serial" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="Label1" runat="server" Text="Application type"></asp:Label>
        <asp:DropDownList ID="Type" runat="server">
            <asp:ListItem Value="1">Pro</asp:ListItem>
            <asp:ListItem Value="2">Plus</asp:ListItem>
            <asp:ListItem Value="3">Plus Home</asp:ListItem>
            <asp:ListItem Value="4">Plus mobile</asp:ListItem>
            <asp:ListItem Value="5">Game Pack</asp:ListItem>
            <asp:ListItem Value="6">Plus Therapy</asp:ListItem>
        </asp:DropDownList>
        <br />
        <asp:Label ID="Label2" runat="server" Text="Application subtype"></asp:Label>
        <asp:DropDownList ID="Subtype" runat="server">
            <asp:ListItem Value="1">Standard</asp:ListItem>
            <asp:ListItem Value="2">Educational</asp:ListItem>
            <asp:ListItem Value="3">Goverment</asp:ListItem>
            <asp:ListItem Value="4">Therapeutist</asp:ListItem>
            <asp:ListItem Value="5">Fun</asp:ListItem>
            <asp:ListItem Value="6">Demo</asp:ListItem>
            <asp:ListItem Value="7">Gamepack (none)</asp:ListItem>
        </asp:DropDownList>
    
    </div>
        <asp:Panel ID="Panel1" runat="server">
            <asp:Label ID="Label3" runat="server" Text="Byte data"></asp:Label>
            <asp:TextBox ID="ByteData" runat="server">5</asp:TextBox>
            <asp:Label ID="Label5" runat="server" Text="0-255"></asp:Label>
            &nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label>
        </asp:Panel>
        <asp:Panel ID="Panel2" runat="server">
            <asp:Label ID="Label7" runat="server" Text="Key for version"></asp:Label>
            <asp:DropDownList ID="VersionType" runat="server">
                <asp:ListItem Value="0">Major</asp:ListItem>
                <asp:ListItem Value="1">Major &amp; minor</asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:Label ID="Label8" runat="server" Text="Version major"></asp:Label>
            <asp:DropDownList ID="Major" runat="server">
                <asp:ListItem>1</asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:Label ID="Label9" runat="server" Text="Version minor"></asp:Label>
            <asp:DropDownList ID="Minor" runat="server">
                <asp:ListItem>1</asp:ListItem>
                <asp:ListItem>2</asp:ListItem>
                <asp:ListItem>3</asp:ListItem>
            </asp:DropDownList>
        </asp:Panel>
        <asp:Label ID="Label10" runat="server" Text="Number keys to generate"></asp:Label>
        <asp:TextBox ID="NumberOfKeys" runat="server">1</asp:TextBox>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Generate" />
        <asp:Panel ID="Panel3" runat="server">
            <br />
        </asp:Panel>
        <asp:Panel ID="Panel4" runat="server">
            <asp:Label ID="Label11" runat="server" Text="Generated keys:"></asp:Label>
        </asp:Panel>
        <asp:Panel ID="Panel5" runat="server">
            <asp:Label ID="LabelKeys" runat="server"></asp:Label>
            <br />
            <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Decode generated" />
            <br />
            <asp:Label ID="LabelDecodeGenerated" runat="server"></asp:Label>
        </asp:Panel>
        <asp:Panel ID="Panel6" runat="server">
            <br />
            <br />
            decode key:<br />
            <asp:TextBox ID="Key" runat="server" Width="397px"></asp:TextBox>
            <br />
            <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Decode" />
            <br />
            <asp:Label ID="DecodedKey" runat="server"></asp:Label>
        </asp:Panel>
    </form>
</body>
</html>
