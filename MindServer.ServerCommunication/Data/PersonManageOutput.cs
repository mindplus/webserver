﻿using System;

namespace MindServer.ServerCommunication.Data
{
    public enum PersonManageResultType
    {
        LoginExists,
        DataNotFound,
        Added,
        Updated,
        Removed,
        ApplicationNotFound
    }
	/// <summary>
	/// Registration output with new registration info
	/// </summary>
	public class PersonManageOutput
	{
        public PersonManageOutput(PersonManageResultType resultType, Int32? userId)
		{
            ResultType = resultType;
            UserId = userId;
		}

		/// <summary>
		/// Application guid
		/// </summary>
        public PersonManageResultType ResultType { get; protected set; }

        /// <summary>
        /// Gets info about registration
        /// </summary>
        public Int32? UserId { get; protected set; }
		
	}
}
