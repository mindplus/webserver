﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MindServer.SqlServer.Model.Data;

namespace MindServer.SqlServer.Model
{
    public class ActivationDbContext : DbContext
    {
		public ActivationDbContext(string connectionString)
            : base(connectionString)
        {
            Configuration.LazyLoadingEnabled = false;
			Database.SetInitializer<ActivationDbContext>(null);//new CreateDatabaseIfNotExists<ActivationDbContext>());
        }

		public DbSet<MindRegisteredApplication> RegisteredApplications { get; set; }

        public DbSet<DbTrace> Traces { get; set; }

        public DbSet<MindSerialUsage> SerialUsages { get; set; }

        public DbSet<MindSerialBlacklisted> BlacklistedSerials { get; set; }
    }
}
