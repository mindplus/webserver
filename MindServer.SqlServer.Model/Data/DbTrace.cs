﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MindServer.SqlServer.Model.Data
{
    [Table("ApplicationTrace")]
    public class DbTrace
    {
        public DbTrace()
        {
            Id = -1;
        }

        public DbTrace(Int32 id, Int32 userId,string internalIp, string publicIp)
        {
            Id = id;
            UserId = userId;
            InternalIp = internalIp;
            PublicIp = publicIp;
        }

        [Key]
        [Column("TraceId")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("PersonId")]
        public Int32 UserId{ get; set; }

        [Column("PublicIpAddress")]
        public string PublicIp { get; set; }

        [Column("InternalIpAddress")]
        public string InternalIp { get; set; }
    }
}
