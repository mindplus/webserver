﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MindServer.SqlServer.Model.Data
{
	[Table("Persons")]
	public class MindPerson
	{
		public MindPerson()
		{
			Id = -1;
		}

        public MindPerson(int id, string type, DateTime registrationDate, string login, string name, string surname, string password,
            string middlename, string email, DateTime? dateOfBirth, string gender, string pin, string country,
            string address, string city, string zip, string phone,
            string physiciansurname, string physicianlogin, 
            string diagnosis, string medication, string othertherapy)
		{
			Id = id;
			Type = type;
		    RegistrationDate = registrationDate;
		    Login = login;
		    Name = name;
		    Surname = surname;
            Password = password;
            Middlename = middlename;
            Email = email;
            DateOfBirth = dateOfBirth;
            Gender = gender;
            Pin = pin;
            CountryCode = country;
            Address = address;
            City = city;
            Zip = zip;
            Phone = phone;
            PhysicianSurname = physiciansurname;
            PhysicianLogin = physicianlogin;
            Diagnosis = diagnosis;
            Medication = medication;
            OtherTherapy = othertherapy;
		}

		[Column("PersonId"), Required, Key, DatabaseGenerated(DatabaseGeneratedOption.Identity) ]
		public Int32 Id { get; protected set; }

		[Column("Type"), Required]
		public string Type { get; protected set; }

        [Column("RegistrationDate"), Required]
        public DateTime RegistrationDate { get; set; }

        [Column("Login"), Required]
        public string Login { get;  set; }

        [Column("Name"), Required]
        public string Name { get;  set; }

        [Column("Surname"), Required]
        public string Surname { get;  set; }

        [Column("Middlename")]
        public string Middlename { get; set; }

        [Column("Password"), Required]
        public string Password { get; set; }

        [Column("Email"), Required]
        public string Email { get; set; }

        [Column("DateOfBirth")]
        public DateTime? DateOfBirth { get; set; }

        [Column("Gender")]
        public string Gender { get; set; }

        [Column("Pin")]
        public string Pin { get; set; }

        [Column("CountryCode")]
        public string CountryCode { get; set; }

        [Column("Address")]
        public string Address { get; set; }

        [Column("City")]
        public string City { get; set; }

        [Column("Zip")]
        public string Zip { get; set; }

        [Column("Phone")]
        public string Phone { get; set; }

       [Column("PhysicianSurname")]
        public string PhysicianSurname { get; set; }

        [Column("PhysicianLogin")]
        public string PhysicianLogin { get; set; }

        [Column("Diagnosis")]
        public string Diagnosis { get; set; }

        [Column("Medication")]
        public string Medication { get; set; }

        [Column("OtherTherapy")]
        public string OtherTherapy { get; set; }
	}
}
