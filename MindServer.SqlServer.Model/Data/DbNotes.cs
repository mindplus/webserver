﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MindServer.SqlServer.Model.Data
{
    [Table("SessionNotesTable")]
    public class DbNotes
    {
        public DbNotes()
        {
            Id = -1;
        }

        public DbNotes(int id, int sessionId, string note, DateTime createDate)
        {
            Id = id;
            SessionId = sessionId;
            Note = note;
            CreateDate = createDate;
        }

        [Key]
        [Column("NoteID")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("SessionId")]
        public int SessionId{ get; set; }

        [Column("Note")]
        public string Note { get; set; }

        [Column("CreateDate")]
        public DateTime CreateDate { get; set; }
    }
}
