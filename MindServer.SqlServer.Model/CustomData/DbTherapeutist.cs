﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MindServer.SqlServer.Model.Data
{
    public class DbTherapeutist
    {
        public DbTherapeutist()
        {
            Id = -1;
        }

        public DbTherapeutist(int id, string login, string password)
        {
            Id = id;
            Login = login;
            Password = password;
        }

        public int Id { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }
    }
}
