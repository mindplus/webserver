﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MindServer.SqlServer.Model.Data
{
	[Table("RegisteredApplications")]
	public class MindRegisteredApplication
	{
		public MindRegisteredApplication()
		{
			Id = -1;
		}

		public MindRegisteredApplication(int id, string guid, DateTime registrationDate, string applicationType, string publicIp, string internalIp, string hardwareData, bool activationNeeded, DateTime? activationDate)
		{
			Id = id;
			Guid = guid;
		    RegistrationDate = registrationDate;
		    ApplicationType = applicationType;
		    PublicIpAddress = publicIp;
		    InternalIpAddress = internalIp;
		    IsActivationNeeded = activationNeeded;
		    HardwareData = hardwareData;
		    ActivationDate = activationDate;
		}

		[Column("RegisteredApplicationId"), Required, Key, DatabaseGenerated(DatabaseGeneratedOption.Identity) ]
		public Int32 Id { get; protected set; }

		[Column("Guid"), Required]
		public string Guid { get; protected set; }

        [Column("RegistrationDate"), Required]
        public DateTime RegistrationDate { get; set; }

        [Column("ApplicationType"), Required]
        public string ApplicationType { get; protected set; }

        [Column("PublicIpAddress"), Required]
        public string PublicIpAddress { get;  set; }

        [Column("InternalIpAddress"), Required]
        public string InternalIpAddress { get;  set; }

        [Column("ActivationNeeded"), Required]
        public bool IsActivationNeeded { get; set; }

        [Column("HardwareData"), Required]
        public string HardwareData { get;  set; }

        [Column("ActivationDate")]
        public DateTime? ActivationDate { get; set; }
	}
}
