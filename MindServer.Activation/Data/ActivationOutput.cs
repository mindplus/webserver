﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MindServer.Activation.Data
{
	/// <summary>
	/// Registration output with new registration info
	/// </summary>
	public class ActivationOutput
	{
        public ActivationOutput(string guid, bool isRegistered, bool isActivationNeeded, bool keyExpired, bool keyBlacklisted, bool keyValid)
		{
			Guid = guid;
            IsRegistered = isRegistered;
            IsActivationNeeded = isActivationNeeded;
            IsKeyExpired = keyExpired;
            IsKeyBlacklisted = keyBlacklisted;
            IskeyValid = keyValid;
		}

		/// <summary>
		/// Application guid
		/// </summary>
		public string Guid { get; protected set; }

        /// <summary>
        /// Gets info about registration
        /// </summary>
        public bool IsRegistered { get; protected set; }

		/// <summary>
		/// Gets info about activation
		/// </summary>
		public bool IsActivationNeeded { get; protected set; }

        /// <summary>
        /// Gets info about activation
        /// </summary>
        public bool IsKeyExpired { get; protected set; }

        /// <summary>
        /// Gets info about activation
        /// </summary>
        public bool IsKeyBlacklisted { get; protected set; }

        /// <summary>
        /// Gets info about activation
        /// </summary>
        public bool IskeyValid { get; protected set; }
	}
}
