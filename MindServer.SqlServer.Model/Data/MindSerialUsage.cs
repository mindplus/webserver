﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MindServer.SqlServer.Model.Data
{
    [Table("Usages")]
    public class MindSerialUsage
    {
        public MindSerialUsage()
        {
            Id = -1;
        }

        public MindSerialUsage(int id, int registeredApplicationId, string serial, int activationCount)
        {
            Id = id;
            RegisteredApplicationId = registeredApplicationId;
            Serial = serial;
            ActivationCount = activationCount;
        }

        [Key]
        [Column("KeyId")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column("RegisteredApplicationId")]
        public int RegisteredApplicationId { get; set; }

        [Column("Serial")]
        public string Serial { get; set; }

        [Column("ActivationCount")]
        public int ActivationCount { get; set; }
    }
}
